/*
 * Created: 2019-09-14
 * Author: zhao.dp (zhao.dp@neusoft.com)
 * Description: 登录/获取用户信息服务
 * -----
 * Last Modified: 2019-09-14 2:56:26 pm
 * Modified By: zhao.dp (zhao.dp@neusoft.com>)
 */
import NCP from '@ncp-web/core'

const ADMIN_CONTEXT = process.env.VUE_APP_ADMIN_URL

// 当前用户信息
const currentUserRes = NCP.createAxios({
  baseURL: ADMIN_CONTEXT + '/getCurrentUser'
})

export function getCurrentUserInfo() {
  return currentUserRes.request({
    method: 'get'
  })
}
