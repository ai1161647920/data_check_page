import dateFormat from 'dateformat'
import NCP from '@ncp-web/core'

export function tableDataFilter(val, rowdata, format) {
  if (!val) return ''
  return dateFormat(new Date(val), format)
}

export function codeFilter(value, type) {
  if (
    value == null ||
    value == undefined ||
    value == '' ||
    type == null ||
    type == undefined ||
    type == ''
  )
    return ''
  type = type.toUpperCase()
  let res
  try {
    res = NCP.codeHandle.selectCodeName(value, type)
    if (res == null || res == undefined) return value
    return res
  } catch (e) {
    return value
  }
}

export function getValueByKey(key, dataItem){
      if(
        key == null || 
        key == undefined || 
        key == ''){
          return '';
      }
    // 通过key获取value
      var len = dataItem.length
      for(var i = 0; i < len; i++){
        //console.log("数据：" + dataItem[i].key + ":" + dataItem[i].value);
        if(dataItem[i].key == key){
          return dataItem[i].value;
        }
      }
      return key;
}
