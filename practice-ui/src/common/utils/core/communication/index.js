export function redirect(menuId) {
  window.parent.postMessage(
    {
      appId: process.env.VUE_APP_ID,
      action: 'redirect',
      content: menuId
    },
    process.env.VUE_APP_PORTAL_URL
  )
}

export function notify(config) {
  window.parent.postMessage(
    {
      appId: process.env.VUE_APP_ID,
      action: 'message',
      content: config
    },
    process.env.VUE_APP_PORTAL_URL
  )
}

export function timeout() {
  window.parent.postMessage(
    {
      appId: process.env.VUE_APP_ID,
      action: 'sessionTimeout'
    },
    process.env.VUE_APP_PORTAL_URL
  )
}
