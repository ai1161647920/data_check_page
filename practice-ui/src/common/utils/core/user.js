import store from '@/store/store.js'
export function getUserInfo() {
  if (process.env.VUE_APP_ENV == 'development') {
    return {
      opterNo: '1002',
      userAcctId: '1002',
      userAcct: 'admin',
      userName: '测试用户名称',
      orgUntId: '34008',
      orgName: '国家医疗保障局',
      orgCodg: '123',
      poolAreaCodg: null,
      admDvs: null,
      prntOrgId: null
    }
  } else {
    return store.getters.user
  }
}
