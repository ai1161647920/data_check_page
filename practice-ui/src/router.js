import Vue from 'vue'
import Router from 'vue-router'
import modulesRoutes from '@/modules/router.js'
import Home from '@/modules/demo/Home.vue'
Vue.use(Router)

// 基础路由信息
const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: { typeList: ['Star_Flag', 'Vali_Flag'] }
  }
]

// 模块路由信息
routes.push(...modulesRoutes)

export default new Router({
  base: process.env.BASE_URL,
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  },
  routes: routes
})
