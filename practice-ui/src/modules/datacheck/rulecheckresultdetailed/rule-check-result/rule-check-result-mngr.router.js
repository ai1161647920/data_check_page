/*
 * Created: 2021-07-21
 * Author: zhouxiaoxin
 * Description:
 * -----
 * Modified: 2021-07-21
 * Modified By: zhouxiaoxin
 * Description:
 */
export default {
  // path 保证全局唯一
  path: '/rule-check-result-detailed',
  component: () =>
    import(
      '@/modules/datacheck/rulecheckresultdetailed/rule-check-result/rule-check-result-mngr.vue'
    ),
  // typeList 中编写模块所需二级代码
  meta: { typeList: [] }
}
