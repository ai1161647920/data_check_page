class RuleCheckResultClass {
  constructor(chkWater, ruleNo, tableName, chkBtch) {
    this.chkWater = chkWater
    this.ruleNo = ruleNo
    this.tableName = tableName
    this.chkBtch = chkBtch
  }
}

export default RuleCheckResultClass
