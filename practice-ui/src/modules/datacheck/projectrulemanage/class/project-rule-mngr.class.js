class ProjectRuleClass {
  constructor(
    ruleGrpNo,
    itemcode,
    dsNo,
    crteTime,
    modiTime,
    crteUser,
    modiUser,
    valiFlag,
    rid,
    dscr
  ) {
    this.ruleGrpNo = ruleGrpNo
    this.itemcode = itemcode
    this.dsNo = dsNo
    this.crteTime = crteTime
    this.modiTime = modiTime
    this.crteUser = crteUser
    this.modiUser = modiUser
    this.valiFlag = valiFlag
    this.rid = rid
    this.dscr = dscr
  }
}

export default ProjectRuleClass
