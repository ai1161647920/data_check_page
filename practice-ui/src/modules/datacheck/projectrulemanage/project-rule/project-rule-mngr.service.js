/*
 * Created: 2021-07-21
 * Author: zhouxiaoxin
 * Description:
 * -----
 * Modified: 2021-07-21
 * Modified By: zhouxiaoxin
 * Description:
 */

import NCP from '@ncp-web/core'
//import { codeFilter, tableDataFilter } from '@/common/filters/index'

// 业务服务上下文，必需设置为 process.env.VUE_APP_BASE_URL
const context = process.env.VUE_APP_BASE_URL

// 针对特定资源，创建 axios 实例
const resourcesConst = NCP.createAxios({
  baseURL: context + '/web/projectrulemanage/projectRule',
  headers: { businessId: 'projectRulebusinessId' }
})
// 针对特定资源，创建 axios 实例
const resourcesProject = NCP.createAxios({
  baseURL: context + '/web/projectmanage/projectInfoB',
  headers: { businessId: 'projectInfoBbusinessId' }
})

const resourcesRule = NCP.createAxios({
  baseURL: context + '/web/rulegroupmanage/ruleGroupB',
  headers: { businessId: 'ruleGroupBbusinessId' }
})

const resourcesDB = NCP.createAxios({
  baseURL: context + '/web/databasemanage/databaseInfoB',
  headers: { businessId: 'databaseInfoBbusinessId' }
})

// 针对特定资源，创建资源访问对象
// 对象变量应同资源同名
const resources = {
  // 好用的POS请求示例
  // return empInsuMergeRes.post('/updateEmpMergeInfo', formEdit, {
  //   headers: { 'Content-Type': 'application/json' }
  // })
  //查询项目
  getDataBases() {
    // 发送请求
    return resourcesDB.request({
      url: '/getDataBases',
      method: 'GET',
      headers: { 'Content-Type': 'application/json' }
    })
  },
  //查询项目
  getItems() {
    // 发送请求
    return resourcesProject.request({
      url: '/getItems',
      method: 'GET',
      headers: { 'Content-Type': 'application/json' }
    })
  },
  //查询项目
  getRules() {
    // 发送请求
    return resourcesRule.request({
      url: '/getRules',
      method: 'GET',
      headers: { 'Content-Type': 'application/json' }
    })
  },
  //查询
  get(projectRuleQuery) {
    // 发送请求
    return resourcesConst.request({
      method: 'GET',
      params: projectRuleQuery,
      headers: { 'Content-Type': 'application/json' }
    })
  },
  // 分页查询
  getByPage(projectRuleQuery, pageConfig) {
    // 发送请求
    return resourcesConst.request({
      url: '/page',
      method: 'GET',
      params: Object.assign({}, pageConfig, projectRuleQuery),
      headers: { 'Content-Type': 'application/json' }
    })
  },
  post(projectRule) {
    return resourcesConst.request({
      method: 'POST',
      data: projectRule,
      headers: { 'Content-Type': 'application/json' }
    })
  },
  put(projectRule) {
    return resourcesConst.request({
      method: 'PUT',
      data: projectRule,
      headers: { 'Content-Type': 'application/json' }
    })
  },
  delete(id) {
    return resourcesConst.request({
      url: '/' + id,
      method: 'DELETE'
    })
  }
}

export default {
  resources
}
