/*
* Created: 2021-07-21
* Author: zhouxiaoxin
* Description:
* -----
* Modified: 2021-07-21
* Modified By: zhouxiaoxin
* Description:
*/

import NCP from '@ncp-web/core'
//import { codeFilter, tableDataFilter } from '@/common/filters/index'

// 业务服务上下文，必需设置为 process.env.VUE_APP_BASE_URL
const context = process.env.VUE_APP_BASE_URL

// 针对特定资源，创建 axios 实例
const resourcesConst = NCP.createAxios({
  baseURL: context + '/web/rulecheckresult/ruleCheckResult',
  headers: { businessId: 'ruleCheckResultbusinessId' }
})

// 针对特定资源，创建资源访问对象
// 对象变量应同资源同名
const resources = {
  // 好用的POS请求示例
  // return empInsuMergeRes.post('/updateEmpMergeInfo', formEdit, {
  //   headers: { 'Content-Type': 'application/json' }
  // })
  
  //查询
  get(ruleCheckResultQuery) {
    // 发送请求
    return resourcesConst.request({
      method: 'GET',
      params: ruleCheckResultQuery,
      headers: { 'Content-Type': 'application/json' }
    })
  },
  // 分页查询
  getByPage(ruleCheckResultQuery, pageConfig) {
    // 发送请求
    return resourcesConst.request({
      url: '/page',
      method: 'GET',
      params: Object.assign({},pageConfig,ruleCheckResultQuery),
      headers: { 'Content-Type': 'application/json' }
    })
  },
  post(ruleCheckResult) {
    return resourcesConst.request({
      method: 'POST',
      data: ruleCheckResult,
      headers: { 'Content-Type': 'application/json' }
    })
  },
  put(ruleCheckResult) {
    return resourcesConst.request({
      method: 'PUT',
      data: ruleCheckResult,
      headers: { 'Content-Type': 'application/json' }
    })
  },
  delete(id) {
    return resourcesConst.request({
      url: '/' + id,
      method: 'DELETE'
    })
  }
}

export default {
  resources
}

