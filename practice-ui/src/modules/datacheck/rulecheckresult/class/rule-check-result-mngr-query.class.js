class RuleCheckResultQueryClass {
  constructor(
    chkWater,
    ruleNo,
    chkSqlCond,
    itemcode,
    totlDataAmt,
    abnDataAmt,
    crtProcer,
    crtOpnn,
    chkBtch,
    crteTime,
    modiTime,
    crteUser,
    modiUser,
    valiFlag,
    rid,
    dscr
  ) {
    this.chkSqlCond = chkSqlCond
    this.chkWater = chkWater
    this.ruleNo = ruleNo
    this.itemcode = itemcode
    this.totlDataAmt = totlDataAmt
    this.abnDataAmt = abnDataAmt
    this.crtProcer = crtProcer
    this.crtOpnn = crtOpnn
    this.chkBtch = chkBtch
    this.crteTime = crteTime
    this.modiTime = modiTime
    this.crteUser = crteUser
    this.modiUser = modiUser
    this.valiFlag = valiFlag
    this.rid = rid
    this.dscr = dscr
  }
}

export default RuleCheckResultQueryClass
