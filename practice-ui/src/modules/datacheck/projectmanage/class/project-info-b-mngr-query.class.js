class ProjectInfoBQueryClass {
  constructor(
    itemcode,
    itemname,
    crteTime,
    modiTime,
    crteUser,
    modiUser,
    valiFlag,
    rid,
    dscr,
  ){
    this.itemcode=itemcode
    this.itemname=itemname
    this.crteTime=crteTime
    this.modiTime=modiTime
    this.crteUser=crteUser
    this.modiUser=modiUser
    this.valiFlag=valiFlag
    this.rid=rid
    this.dscr=dscr
  }
}

export default ProjectInfoBQueryClass
