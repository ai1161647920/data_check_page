/*
* Created: 2021-07-21
* Author: zhouxiaoxin
* Description:
* -----
* Modified: 2021-07-21
* Modified By: zhouxiaoxin
* Description:
*/
export default {
// path 保证全局唯一
  path: '/rule-info-b',
  component: () => import('@/modules/datacheck/ruleinfomanage/rule-info-b/rule-info-b-mngr.vue'),
// typeList 中编写模块所需二级代码
  meta: { typeList: [
    ] }
}
