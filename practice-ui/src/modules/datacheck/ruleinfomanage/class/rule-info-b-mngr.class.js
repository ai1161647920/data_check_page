class RuleInfoBClass {
  constructor(
    ruleNo,
    ruleGrpNo,
    tabUser,
    tabName,
    tabAtn,
    chkSqlCond,
    ruleType,
    chkFlag,
    crteTime,
    modiTime,
    crteUser,
    modiUser,
    valiFlag,
    rid,
    dscr,
    ruleNote,
    ruleDec,
  ) {
    this.ruleNo=ruleNo
    this.ruleGrpNo=ruleGrpNo
    this.tabUser=tabUser
    this.tabName=tabName
    this.tabAtn=tabAtn
    this.chkSqlCond=chkSqlCond
    this.ruleType=ruleType
    this.chkFlag=chkFlag
    this.crteTime=crteTime
    this.modiTime=modiTime
    this.crteUser=crteUser
    this.modiUser=modiUser
    this.valiFlag=valiFlag
    this.rid=rid
    this.dscr=dscr
    this.ruleNote=ruleNote
    this.ruleDec=ruleDec
  }
}

export default RuleInfoBClass
