class DatabaseInfoBQueryClass {
  constructor(
    dsNo,
    ds,
    dsName,
    item,
    userName,
    pwd,
    connectstring,
    driver,
    crteTime,
    modiTime,
    crteUser,
    modiUser,
    valiFlag,
    rid,
    dscr,
  ){
    this.dsNo=dsNo
    this.ds=ds
    this.dsName=dsName
    this.item=item
    this.userName=userName
    this.pwd=pwd
    this.connectstring=connectstring
    this.driver=driver
    this.crteTime=crteTime
    this.modiTime=modiTime
    this.crteUser=crteUser
    this.modiUser=modiUser
    this.valiFlag=valiFlag
    this.rid=rid
    this.dscr=dscr
  }
}

export default DatabaseInfoBQueryClass
