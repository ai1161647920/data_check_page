/*
* Created: 2021-07-21
* Author: zhouxiaoxin
* Description:
* -----
* Modified: 2021-07-21
* Modified By: zhouxiaoxin
* Description:
*/
export default {
// path 保证全局唯一
  path: '/rule-group-b',
  component: () => import('@/modules/datacheck/rulegroupmanage/rule-group-b/rule-group-b-mngr.vue'),
// typeList 中编写模块所需二级代码
  meta: { typeList: [
    ] }
}
