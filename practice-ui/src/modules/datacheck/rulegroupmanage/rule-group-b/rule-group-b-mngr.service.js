/*
 * Created: 2021-07-21
 * Author: zhouxiaoxin
 * Description:
 * -----
 * Modified: 2021-07-21
 * Modified By: zhouxiaoxin
 * Description:
 */

import NCP from '@ncp-web/core'
//import { codeFilter, tableDataFilter } from '@/common/filters/index'

// 业务服务上下文，必需设置为 process.env.VUE_APP_BASE_URL
const context = process.env.VUE_APP_BASE_URL

// 针对特定资源，创建 axios 实例
const resourcesConst = NCP.createAxios({
  baseURL: context + '/web/rulegroupmanage/ruleGroupB',
  headers: { businessId: 'ruleGroupBbusinessId' }
})
// 针对特定资源，创建 axios 实例
const resourcesProject = NCP.createAxios({
  baseURL: context + '/web/projectmanage/projectInfoB',
  headers: { businessId: 'projectInfoBbusinessId' }
})


const resourcesDB = NCP.createAxios({
  baseURL: context + '/web/databasemanage/databaseInfoB',
  headers: { businessId: 'databaseInfoBbusinessId' }
})

// 针对特定资源，创建资源访问对象
// 对象变量应同资源同名
const resources = {
  // 好用的POS请求示例
  // return empInsuMergeRes.post('/updateEmpMergeInfo', formEdit, {
  //   headers: { 'Content-Type': 'application/json' }
  // })
//查询项目
getDataBases() {
  // 发送请求
  return resourcesDB.request({
    url: '/getDataBases',
    method: 'GET',
    headers: { 'Content-Type': 'application/json' }
  })
},
//查询项目
getItems() {
  // 发送请求
  return resourcesProject.request({
    url: '/getItems',
    method: 'GET',
    headers: { 'Content-Type': 'application/json' }
  })
},
  //查询
  get(ruleGroupBQuery) {
    // 发送请求
    return resourcesConst.request({
      method: 'GET',
      params: ruleGroupBQuery,
      headers: { 'Content-Type': 'application/json' }
    })
  },
  // 分页查询
  getByPage(ruleGroupBQuery, pageConfig) {
    // 发送请求
    return resourcesConst.request({
      url: '/page',
      method: 'GET',
      params: Object.assign({}, pageConfig, ruleGroupBQuery),
      headers: { 'Content-Type': 'application/json' }
    })
  },
  post(ruleGroupB) {
    return resourcesConst.request({
      method: 'POST',
      data: ruleGroupB,
      headers: { 'Content-Type': 'application/json' }
    })
  },
  put(ruleGroupB) {
    return resourcesConst.request({
      method: 'PUT',
      data: ruleGroupB,
      headers: { 'Content-Type': 'application/json' }
    })
  },
  delete(ruleGroupB) {
    return resourcesConst.request({
      url: '/delete',
      method: 'DELETE',
      data: ruleGroupB,
      headers: { 'Content-Type': 'application/json' }
    })
  }
}

export default {
  resources
}
