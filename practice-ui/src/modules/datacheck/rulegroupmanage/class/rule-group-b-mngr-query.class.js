class RuleGroupBQueryClass {
  constructor(
    ruleGrpNo,
    dsNo,
    id,
    itemCode,
    grpName,
    crteTime,
    modiTime,
    crteUser,
    modiUser,
    valiFlag,
    rid,
    dscr
  ) {
    this.id = id
    this.dsNo = dsNo
    this.itemCode = itemCode
    this.ruleGrpNo = ruleGrpNo
    this.grpName = grpName
    this.crteTime = crteTime
    this.modiTime = modiTime
    this.crteUser = crteUser
    this.modiUser = modiUser
    this.valiFlag = valiFlag
    this.rid = rid
    this.dscr = dscr
  }
}

export default RuleGroupBQueryClass
