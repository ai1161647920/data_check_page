/*
* Created: 2021-07-26
* Author: pengli
* Description:
* -----
* Modified: 2021-07-26
* Modified By: pengli
* Description:
*/
export default {
// path 保证全局唯一
  path: '/job-task',
  component: () => import('@/modules/datacheck/jobtaskmanage/job-task/job-task-mngr.vue'),
// typeList 中编写模块所需二级代码
  meta: { typeList: [
    ] }
}
