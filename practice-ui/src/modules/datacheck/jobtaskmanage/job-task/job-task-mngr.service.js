/*
* Created: 2021-07-26
* Author: pengli
* Description:
* -----
* Modified: 2021-07-26
* Modified By: pengli
* Description:
*/

import NCP from '@ncp-web/core'
//import { codeFilter, tableDataFilter } from '@/common/filters/index'

// 业务服务上下文，必需设置为 process.env.VUE_APP_BASE_URL
const context = process.env.VUE_APP_BASE_URL

// 针对特定资源，创建 axios 实例
const resourcesConst = NCP.createAxios({
  baseURL: context + '/web/jobtaskmanage/jobTask',
  headers: { businessId: 'jobTaskbusinessId' }
})

const scheduledResourcesConst = NCP.createAxios({
  baseURL: context + '/web/jobtaskmanage/scheduled',
  headers: { businessId: 'jobTaskbusinessId' }
})

// 针对特定资源，创建资源访问对象
// 对象变量应同资源同名
const resources = {
  // 好用的POS请求示例
  // return empInsuMergeRes.post('/updateEmpMergeInfo', formEdit, {
  //   headers: { 'Content-Type': 'application/json' }
  // })
  
  //查询
  get(jobTaskQuery) {
    // 发送请求
    return resourcesConst.request({
      method: 'GET',
      params: jobTaskQuery,
      headers: { 'Content-Type': 'application/json' }
    })
  },
  // 根据taskNo查询
  queryByTaskNo(taskNo){
       return resourcesConst.request({
         url:'/' + taskNo,
         method: 'GET',
         headers: { 'Content-Type': 'application/json' }
       })

      },
  // 分页查询
  getByPage(jobTaskQuery, pageConfig) {
    // 发送请求
    return resourcesConst.request({
      url: '/page',
      method: 'GET',
      params: Object.assign({},pageConfig,jobTaskQuery),
      headers: { 'Content-Type': 'application/json' }
    })
  },
  post(jobTask) {
    return resourcesConst.request({
      method: 'POST',
      data: jobTask,
      headers: { 'Content-Type': 'application/json' }
    })
  },
  put(jobTask) {
    return resourcesConst.request({
      method: 'PUT',
      data: jobTask,
      headers: { 'Content-Type': 'application/json' }
    })
  },  
  startTask(jobTask) {
    return scheduledResourcesConst.request({
      url: '/start',
      method: 'POST',
      data: jobTask,
      headers: { 'Content-Type': 'application/json' }
    })
  },
  execTask(jobTask) {
    return scheduledResourcesConst.request({
      url: '/execute',
      method: 'POST',
      data: jobTask,
      headers: { 'Content-Type': 'application/json' }
    })
  },
  stopTask(jobTask) {
    return scheduledResourcesConst.request({
      url: '/stop',
      method: 'POST',
      data: jobTask,
      headers: { 'Content-Type': 'application/json' }
    })
  },
  delete(id) {
    return resourcesConst.request({
      url: '/' + id,
      method: 'DELETE'
    })
  }
}

export default {
  resources
}

