class JobTaskQueryClass {
  constructor(
    taskNo,
    taskName,
    itemname,
    exeRuleScp,
    jobCorn,
    timrStas,
    lastExeRslt,
    crteTime,
    modiTime,
    crteUser,
    modiUser,
    valiFlag,
    rid,
    dscr,
  ){
    this.taskNo=taskNo
    this.taskName=taskName
    this.itemname=itemname
    this.exeRuleScp=exeRuleScp
    this.jobCorn=jobCorn
    this.timrStas=timrStas
    this.lastExeRslt=lastExeRslt
    this.crteTime=crteTime
    this.modiTime=modiTime
    this.crteUser=crteUser
    this.modiUser=modiUser
    this.valiFlag=valiFlag
    this.rid=rid
    this.dscr=dscr
  }
}

export default JobTaskQueryClass
