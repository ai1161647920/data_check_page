class PsnInfoBClass {
  constructor(
    psnNo,
    brdy,
    certNo,
    tel,
    naty,
    addr,
    psnName,
    gend,
    insutype,
    empName,
    insuOptins,
    empCode,
  ) {
    this.psnNo=psnNo
    this.brdy=brdy
    this.certNo=certNo
    this.tel=tel
    this.naty=naty
    this.addr=addr
    this.psnName=psnName
    this.gend=gend
    this.insutype=insutype
    this.empName=empName
    this.insuOptins=insuOptins
    this.empCode=empCode
  }
}

export default PsnInfoBClass
