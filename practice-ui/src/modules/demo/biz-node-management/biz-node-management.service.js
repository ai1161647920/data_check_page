/*
 * Created: 2019-08-31
 * Author: zhao.dp (zhao.dp@neusoft.com)
 * Description:
 *    国家医保局项目标准 service 规范代码示例
 *    后台接口设计应严格遵循 RESTful 接口规范，
 *    个别特殊情况可以不严格遵循。
 *    具体 RESTful 规范请参考《RESTful 接口规范》
 * -----
 * Last Modified: 2019-09-02 11:24:48 am
 * Modified By: zhao.dp (zhao.dp@neusoft.com>)
 */

import NCP from '@ncp-web/core'
import { codeFilter, tableDataFilter } from '@/common/filters/index'

// 业务服务上下文，必需设置为 process.env.VUE_APP_BASE_URL
const context = process.env.VUE_APP_BASE_URL

// 针对特定资源，创建 axios 实例
const bizNodeRes = NCP.createAxios({
  baseURL: context + '/node',
  headers: { servMattNodeNo: '123', reqId: '456' }
})

// 针对特定资源，创建资源访问对象
// 对象变量应同资源同名
const bizNode = {
  // 分页查询
  getByPage(queryForm, pageConfig) {
    // 发送请求
    return bizNodeRes.request({
      url: '/page',
      method: 'GET',
      params: Object.assign({}, pageConfig, queryForm)
    })
  },
  post(bizNode) {
    return bizNodeRes.request({
      method: 'POST',
      data: bizNode,
      headers: { 'Content-Type': 'application/json' }
    })
  },
  delete(bizNodeNo) {
    return bizNodeRes.request({
      url: '/' + bizNodeNo,
      method: 'DELETE'
    })
  },
  put(bizNode) {
    return bizNodeRes.request({
      method: 'PUT',
      data: bizNode,
      headers: { 'Content-Type': 'application/json' }
    })
  },
  saveCascade(servItem, bizNodeList) {
    return bizNodeRes.request({
      url: '/saveCascade',
      method: 'POST',
      data: { itemDO: servItem, nodeList: bizNodeList },
      headers: { 'Content-Type': 'application/json' }
    })
  }
}

// 针对特定资源，创建 axios 实例
const servItemRes = NCP.createAxios({
  baseURL: context + '/serv/getServById'
})

// 针对特定资源，创建资源访问对象
const servItem = {
  get(itemNo) {
    return servItemRes.request({
      method: 'GET',
      params: { servItemNo: itemNo }
    })
  }
}

// 前后台交互 DTO 类型定义

/**
 * 业务环节信息表
 *
 * @author zhao.dp
 * @date 2019-09-02
 * @class BizNode
 */
class BizNode {
  // 字段需写清楚对应的汉语注释
  constructor(
    bizNodeNo, // 业务环节编号
    valiFlag, // 有效标志
    memo, // 备注
    bizOptins, // 业务经办机构
    bizOptSn, // 业务经办流水号
    bizNo, // 业务编号
    servItemNo, // 服务事项编号
    optTimelmt, // 经办时限
    bizNodeName, // 业务环节名称
    bizNodeDesc, // 业务环节说明
    bizNodeRlsTime, // 业务环节发布时间
    nodeSn, //节点序号
    bizRechkLv //业务复核等级
  ) {
    this.bizNodeNo = bizNodeNo
    this.valiFlag = valiFlag
    this.memo = memo
    this.bizOptins = bizOptins
    this.bizOptSn = bizOptSn
    this.bizNo = bizNo
    this.servItemNo = servItemNo
    this.optTimelmt = optTimelmt
    this.bizNodeName = bizNodeName
    this.bizNodeDesc = bizNodeDesc
    this.bizNodeRlsTime = bizNodeRlsTime
    this.nodeSn = nodeSn
    this.bizRechkLv = bizRechkLv
  }
}

/**
 * 服务事项信息表
 *
 * @author zhao.dp
 * @date 2019-09-02
 * @class ServItem
 */
class ServItem {
  constructor(
    servItemNo, // 服务事项编号
    valiFlag, // 有效标志
    memo, // 备注
    bizOptins, // 业务经办机构
    bizOptSn, // 业务经办流水号
    servItemName, // 服务事项名称
    servItemDesc, // 服务事项说明
    sysNo, // 系统编号
    sysName, // 系统名称
    servItemRlsTime // 服务事项发布时间
  ) {
    this.servItemNo = servItemNo
    this.valiFlag = valiFlag
    this.memo = memo
    this.bizOptins = bizOptins
    this.bizOptSn = bizOptSn
    this.servItemName = servItemName
    this.servItemDesc = servItemDesc
    this.sysNo = sysNo
    this.sysName = sysName
    this.servItemRlsTime = servItemRlsTime
  }
}

// ncp-table 列定义对象可以定义在 service 中，
// 也可以定义在 vue 文件中。两种风格都可以接受。
const columnDefs = [
  {
    label: '业务环节编号',
    prop: 'bizNodeNo',
    width: '120px',
    fixed: 'left'
  },
  {
    label: '有效标志',
    prop: 'valiFlag',
    filters: [
      {
        filter: cellValue => {
          return codeFilter(cellValue, 'Vali_Flag')
        }
      }
    ],
    fixed: 'left'
  },
  {
    label: '备注',
    prop: 'memo'
  },
  {
    label: '业务经办机构',
    prop: 'bizOptins',
    width: '120px'
  },
  {
    label: '业务经办流水号',
    prop: 'bizOptSn',
    width: '120px'
  },
  {
    label: '业务编号',
    prop: 'bizNo'
  },
  {
    label: '服务事项编号',
    prop: 'servItemNo',
    width: '120px'
  },
  {
    label: '经办时限',
    prop: 'optTimelmt',
    filters: [{ filter: tableDataFilter, params: ['yyyy年mm月dd日'] }],
    width: '150px'
  },
  {
    label: '业务环节名称',
    prop: 'bizNodeName',
    width: '140px'
  },
  {
    label: '业务环节说明',
    prop: 'bizNodeDesc',
    width: '140px'
  },
  {
    label: '业务环节发布时间',
    prop: 'bizNodeRlsTime',
    width: '140px',
    filters: [{ filter: tableDataFilter, params: ['yyyy年mm月dd日'] }]
  },
  {
    label: '节点序号',
    prop: 'nodeSn'
  },
  {
    label: '业务复核等级',
    prop: 'bizRechkLv',
    width: '120px'
  }
]

// 导出对象统一定义在 service 的最下方
// 统一导出 default 对象
export default {
  // DTO 类型一律以 type 属性导出
  type: { BizNode, ServItem },
  // 资源访问对象一律以 resources 属性导出
  resources: { bizNode, servItem },
  // 如果 ncp-table 列定义对象定义在了 service 中，则以 columnDefs 属性导出
  columnDefs
}
