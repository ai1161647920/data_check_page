/*
 * Created: 2019-09-6
 * Author: zhao.dp (zhao.dp@neusoft.com)
 * Description:
 *    国家医保局项目标准路由规范代码示例
 *    其他关于路由使用的规范，请参考《路由使用规范》文档
 * -----
 * Last Modified: 2019-09-09 7:30:32 pm
 * Modified By: zhao.dp (zhao.dp@neusoft.com>)
 */
export default {
  // path 保证全局唯一
  path: '/biz-node-management',
  component: () => import('./biz-node-management.vue'),
  // typeList 中编写模块所需二级代码
  meta: { typeList: ['Vali_Flag'] }
}
