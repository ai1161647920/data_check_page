export default {
  path: '/receiver',
  component: () => import('./receiver.vue'),
  props: route => {
    return { portalData: route.query.data }
  }
}
