import NCP from '@ncp-web/core'
const context = process.env.VUE_APP_BASE_URL
const pollingRes = NCP.createAxios({
  baseURL: context + '/polling'
})
const polling = {
  get() {
    return pollingRes.request({
      method: 'GET',
      headers: { polling: true }
    })
  }
}

export default {
  resources: { polling }
}
