/*
 * @Author: jiachw
 * @Date: 2019-12-06 10:14:56
 * @LastEditTime: 2019-12-06 10:39:24
 * @LastEditors: jiachw
 * @Description:element 表单校验工具
 */
import { elValidatorWrapper } from '@ncp-web/util'
import moment from 'moment'
/**
 * 校验年度不能大于输出年度，默认为今年
 */
export function withinYear(year) {
  year = year || moment().year()
  return elValidatorWrapper(val => val <= year, `选择年度不能大于${year}年`)
}
