export default {
  path: '/file-download',
  name: 'file-download',
  component: () =>
    import(
      /* webpackChunkName: "person-manager" */ '@/modules/demo/file-download/file-download.vue'
    )
}
