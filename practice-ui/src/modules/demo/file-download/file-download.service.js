/*
 * Created: 2019-08-16
 * Author: zhao.dp (zhao.dp@neusoft.com)
 * Description: person-manager service
 * -----
 * Last Modified: 2019-08-17 10:30:59 am
 * Modified By: zhao.dp (zhao.dp@neusoft.com>)
 */
import NCP from '@ncp-web/core'

// 业务服务上下文
const context = process.env.VUE_APP_BASE_URL

// 针对特定资源，创建 axios 实例
const fileRes = NCP.createAxios({
  baseURL: context + '/file/test.docx'
})

const file = {
  /**
   * 给后台发送请求，下载文件
   *
   * @author zhao.dp
   * @date 2019-08-31
   * @returns
   */
  get() {
    return fileRes.request({
      method: 'GET',
      responseType: 'blob'
    })
  }
}

export default { file }
