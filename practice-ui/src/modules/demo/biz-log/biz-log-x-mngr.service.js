/*
 * Created: 2020-04-08
 * Author: pang_y
 * Description:
 * -----
 * Modified: 2020-04-08
 * Modified By: pang_y
 * Description:
 */

import NCP from '@ncp-web/core'
// import { codeFilter, tableDataFilter } from '@/common/filters/index'

// 业务服务上下文，必需设置为 process.env.VUE_APP_BASE_URL
const context = process.env.VUE_APP_BASE_URL

// 针对特定资源，创建 axios 实例
const bizLogXRes = NCP.createAxios({
  baseURL: context + '/idempotent/bizLogX',
  headers: { businessId: 'bizLogXbusinessId' }
})

// 针对特定资源，创建资源访问对象
// 对象变量应同资源同名
const bizLogX = {
  //查询
  get(bizLogXQuery) {
    // 发送请求
    return bizLogXRes.request({
      method: 'GET',
      params: bizLogXQuery,
      headers: { 'Content-Type': 'application/json' }
    })
  },
  // 分页查询
  getByPage(bizLogXQuery, pageConfig) {
    // 发送请求
    return bizLogXRes.request({
      url: '/page',
      method: 'GET',
      params: Object.assign({}, pageConfig, bizLogXQuery),
      headers: { 'Content-Type': 'application/json' }
    })
  },
  post(bizLogX) {
    return bizLogXRes.request({
      method: 'POST',
      data: bizLogX,
      headers: { 'Content-Type': 'application/json' }
    })
  },
  put(bizLogX) {
    return bizLogXRes.request({
      method: 'PUT',
      data: bizLogX,
      headers: { 'Content-Type': 'application/json' }
    })
  },
  delete(id) {
    return bizLogXRes.request({
      url: '/' + id,
      method: 'DELETE'
    })
  }
}

export default {
  resources: { bizLogX }
}
