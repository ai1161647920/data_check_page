class BizLogXQueryClass {
  constructor(
    bizSn,
    ptcp,
    ptcpName,
    ptcpType,
    ptcpPoolarea,
    ptcpOptins,
    opter,
    opterName,
    optTime,
    optins,
    poolarea,
    servItemNodeNo,
    reqId
  ) {
    this.bizSn = bizSn
    this.ptcp = ptcp
    this.ptcpName = ptcpName
    this.ptcpType = ptcpType
    this.ptcpPoolarea = ptcpPoolarea
    this.ptcpOptins = ptcpOptins
    this.opter = opter
    this.opterName = opterName
    this.optTime = optTime
    this.optins = optins
    this.poolarea = poolarea
    this.servItemNodeNo = servItemNodeNo
    this.reqId = reqId
  }
}

export default BizLogXQueryClass
