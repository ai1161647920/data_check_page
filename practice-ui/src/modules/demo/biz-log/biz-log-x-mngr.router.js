/*
 * Created: 2020-04-08
 * Author: pang_y
 * Description:
 * -----
 * Modified: 2020-04-08
 * Modified By: pang_y
 * Description:
 */
export default {
  // path 保证全局唯一
  path: '/biz-log-x',
  component: () => import('@/modules/demo/biz-log/biz-log-x-mngr.vue'),
  // typeList 中编写模块所需二级代码
  meta: { typeList: ['ptcpType'] }
}
