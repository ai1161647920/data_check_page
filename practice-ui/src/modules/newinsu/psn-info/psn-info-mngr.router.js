/*
* Created: 2020-12-28
* Author: zhouxiaoxin
* Description:
* -----
* Modified: 2020-12-28
* Modified By: zhouxiaoxin
* Description:
*/
export default {
// path 保证全局唯一
  path: '/psn-info',
  component: () => import('@/modules/newinsu/psn-info/psn-info-mngr.vue'),
// typeList 中编写模块所需二级代码
  meta: { typeList: [
    ] }
}
