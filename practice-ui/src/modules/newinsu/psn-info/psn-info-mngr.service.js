/*
* Created: 2020-12-28
* Author: zhouxiaoxin
* Description:
* -----
* Modified: 2020-12-28
* Modified By: zhouxiaoxin
* Description:
*/

import NCP from '@ncp-web/core'
//import { codeFilter, tableDataFilter } from '@/common/filters/index'

// 业务服务上下文，必需设置为 process.env.VUE_APP_BASE_URL
const context = process.env.VUE_APP_BASE_URL

// 针对特定资源，创建 axios 实例 请求psn
const resourcesConstPsn = NCP.createAxios({
  baseURL: context + '/web/newinsu/psnInfo',
  headers: { businessId: 'psnInfobusinessId' }
})

// 针对特定资源，创建 axios 实例 请求reflAppyD
const resourcesConstrefl = NCP.createAxios({
  baseURL: context + '/web/newinsu/reflAppyD',
  headers: { businessId: 'reflRegbusinessId' }
})

// 针对特定资源，创建 axios 实例 请求reflAppyD
const resourcesFixmedins = NCP.createAxios({
  baseURL: context + '/web/newinsu/fixmedins',
  headers: { businessId: 'fixmedinsbusinessId' }
})

// 针对特定资源，创建资源访问对象
// 对象变量应同资源同名
const resources = {
  // 好用的POS请求示例
  // return empInsuMergeRes.post('/updateEmpMergeInfo', formEdit, {
  //   headers: { 'Content-Type': 'application/json' }
  // })

  //模糊查询定点机构编码
  getFisById(fixId) {
    // 发送请求
    return resourcesFixmedins.request({
      url: '/getFisById',
      method: 'GET',
      params: { fixId: fixId },
      headers: { 'Content-Type': 'application/json' }
    })
  },

  //根据id查询定点机构数据
  getFixById(fixId) {
    // 发送请求
    return resourcesFixmedins.request({
      url: '/getFixById',
      method: 'GET',
      params: { fixId: fixId },
      headers: { 'Content-Type': 'application/json' }
    })
  },

  //根据人员id查询
  getById(psnId) {
    // 发送请求
    return resourcesConstPsn.request({
      url: '/getPsnById',
      method: 'GET',
      params: { psnId: psnId },
      headers: { 'Content-Type': 'application/json' }
    })
  },


  // 分页查询
  getReflRegByPage(psnNo, pageConfig) {
    // 发送请求
    return resourcesConstrefl.request({
      url: '/page',
      method: 'GET',
      params: Object.assign({}, pageConfig, { psnNo: psnNo }),
      headers: { 'Content-Type': 'application/json' }
    })
  },

  post(psnInfo) {
    return resourcesConstrefl.request({
      method: 'POST',
      data: psnInfo,
      headers: { 'Content-Type': 'application/json' }
    })
  },
  put(psnInfo) {
    return resourcesConstrefl.request({
      method: 'PUT',
      data: psnInfo,
      headers: { 'Content-Type': 'application/json' }
    })
  },
  delete(id) {
    return resourcesConstrefl.request({
      url: '/' + id,
      method: 'DELETE'
    })
  }
}

export default {
  resources
}

