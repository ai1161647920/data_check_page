class ReflRegClass {
  constructor(
    psnNo,
    trtDclaDetlSn,
    dclaSouc,
    fixmedinsCode,
    fixmedinsName,
    medinsLv,
    fixmedinsPoolarea,
    diseCode,
    diseName,
    diseCondDscr,
    reflinMedinsNo,
    reflinMedinsName,
    outFlag,
    reflDate,
    reflRea,
    reflSetlFlag,
    agntName,
    agntCertType,
    agntCertno,
    agntTel,
    agntAddr,
    begndate,
    enddate,
    valiFlag,
    memo,
    updtTime,
    apprFlag
  ) {
    this.psnNo = psnNo
    this.trtDclaDetlSn = trtDclaDetlSn
    this.dclaSouc = dclaSouc
    this.fixmedinsCode = fixmedinsCode
    this.fixmedinsName = fixmedinsName
    this.medinsLv = medinsLv
    this.fixmedinsPoolarea = fixmedinsPoolarea
    this.diseCode = diseCode
    this.diseName = diseName
    this.diseCondDscr = diseCondDscr
    this.reflinMedinsNo = reflinMedinsNo
    this.reflinMedinsName = reflinMedinsName
    this.outFlag = outFlag
    this.reflDate = reflDate
    this.reflRea = reflRea
    this.reflSetlFlag = reflSetlFlag
    this.agntName = agntName
    this.agntCertType = agntCertType
    this.agntCertno = agntCertno
    this.agntTel = agntTel
    this.agntAddr = agntAddr
    this.begndate = begndate
    this.enddate = enddate
    this.valiFlag = valiFlag
    this.memo = memo
    this.updtTime = updtTime
    this.apprFlag = apprFlag
  }
}

export default ReflRegClass
