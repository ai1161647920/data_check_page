import '@babel/polyfill'
// 核心组件
import Vue from 'vue'
import App from './App'
import router from './router'
import ncpElement from '@ncp-web/common-ui'
import hsaElement from '@ncp-web/hsa-ui'
import Element from 'element-ui'
// 工程初始化
import '@/framework/app-init.js'
// store 必需要放在工程初始化之后执行，否则拦截器不生效
import store from './store/store'
// 路由守卫
import '@/framework/router-guard/router-guard.js'
// 图标
import '@/common/icon/index.js'
// 样式
import '@/style/index.js'
// 过滤器
import * as filters from './common/filters'
// 统一框架组件
import NCP from '@ncp-web/core'
// 医保局行业线工具
import '@/common/utils/core/index.js'

Vue.use(ncpElement)
Vue.use(hsaElement)
Vue.use(Element)

Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

// 绑定NCP对象
Vue.prototype.$ncp = NCP

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
