var fs = require('fs')
var Path = require('path')
var archiver = require('archiver')
const cmdArgs = process.argv.slice(2)
const warTempFolder = getTempFolderName()
const warFileName = `${Path.parse(__dirname).base}.war`

//兼容将前台文件打包到war包根路径下的情况
let targetRelativePath = cmdArgs[1] && cmdArgs[1] !== './' ? cmdArgs[1] : ''
const codeTargetPath = warTempFolder + targetRelativePath
const codeSourcePath = cmdArgs[0]
const webInfDirs = [
  warTempFolder + 'WEB-INF/classes',
  warTempFolder + 'WEB-INF/lib'
]
const webXmlPath = warTempFolder + 'WEB-INF/web.xml'
const webXmlContent =
  '<?xml version="1.0" ?>\n' +
  '<!DOCTYPE web-app PUBLIC "-//Sun Microsystems, Inc.//DTD Web Application 2.3//EN" "http://java.sun.com/dtd/web-app_2_3.dtd">\n' +
  '\n' +
  '<web-app>\n' +
  '\t<display-name>Struts Blank Application</display-name>\n' +
  '\n' +
  '\t<!-- The Usual Welcome File List -->\n' +
  '\t<welcome-file-list>\n' +
  '\t\t<welcome-file>index.html</welcome-file>\n' +
  '\t</welcome-file-list>\n' +
  '</web-app>'

function getTempFolderName() {
  let baseName = '.war.tmp.'
  for (let i = 0; ; i++) {
    if (!fs.existsSync(baseName + i)) {
      return baseName + i + '/'
    }
  }
}

/*
 * 同步复制目录、子目录，及其中的文件
 * @param src {String} 要复制的目录
 * @param dist {String} 复制到目标目录
 */
function copyDir(src, dist) {
  if (!fs.existsSync(dist)) {
    fs.mkdirSync(dist)
  }
  var children = fs.readdirSync(src)
  children.forEach(function(child) {
    var _src = src + '/' + child
    var _dist = dist + '/' + child
    var stat = fs.statSync(_src)
    if (stat.isFile()) {
      // 如果是文件，直接复制
      fs.writeFileSync(_dist, fs.readFileSync(_src))
    } else if (stat.isDirectory()) {
      // 如果是目录，递归复制
      copyDir(_src, _dist)
    }
  })
}

const deleteFolderRecursive = function(path) {
  if (fs.existsSync(path)) {
    fs.readdirSync(path).forEach(file => {
      const curPath = Path.join(path, file)
      if (fs.lstatSync(curPath).isDirectory()) {
        // recurse
        deleteFolderRecursive(curPath)
      } else {
        // delete file
        fs.unlinkSync(curPath)
      }
    })
    fs.rmdirSync(path)
  }
}

function createWar() {
  let outputFile = fs.createWriteStream(__dirname + '/' + warFileName)
  let archive = archiver('zip')
  archive.pipe(outputFile)
  outputFile.on('close', function() {
    console.log(archive.pointer() + ' total bytes')
    //压缩完成后删除临时文件目录
    deleteFolderRecursive(warTempFolder)
    console.log('workspace cleared')
  })
  // This event is fired when the data source is drained no matter what was the data source.
  // It is not part of this library but rather from the NodeJS Stream API.
  // @see: https://nodejs.org/api/stream.html#stream_event_end
  outputFile.on('end', function() {
    console.log('Data has been drained')
  })
  archive.directory(warTempFolder, false)
  archive.finalize()
}

//创建war包的代码文件夹
fs.mkdirSync(codeTargetPath, { recursive: true })
//将代码拷贝到war包的代码文件夹中
copyDir(codeSourcePath, codeTargetPath)
//创建WEB-INF相关文件夹
webInfDirs.forEach(item => fs.mkdirSync(item, { recursive: true }))
//创建web.xml文件
fs.writeFileSync(webXmlPath, webXmlContent)
//压缩为zip包
createWar()
