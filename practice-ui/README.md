# 医疗保障行业线种子工程

医疗保障行业线种子工程。各医保行业线工程可基于此工程创建。

## 1. 使用

> 在执行以下命令前，请确保完成了 *环境配置* 文档中的内容

### 1.1 依赖安装

```
npm i
```

### 1.2 启动开发服务器（支持HMR）

```
npm run serve
```

服务启动后在浏览器地址栏中输入：`http://localhost:8088/#/biz-node-management` 查看综合示例

## 2. 工程本地化

工程本地化步骤：

1. 删除 src/modules 文件夹下除 router.js 以外的文件
2. 修改 .env.production 与 .env.development  中 VUE_APP_ID (工程唯一标识)
3. 修改 .env.production 与 .env.development  中 VUE_APP_TITLE (工程中文名)
4. package.json 中 name 属性修改成与 VUE_APP_ID 同样的值 
5. .env.production / .env.development 中 VUE_APP_BASE_URL 根据后端API统一重写
6. vue.config.js devServer.proxy 重写
7. README.md 重写

## 3. 阿里云部署构建 war 包

1. 修改 Jenkins 的发布命令，在`npm run build`命令之后，添加下面的命令：
   
   ```
   node buildWar ${sourcePath} ${targetPath}
   ```
   
   其中`${sourcePath}`为代码的相对路径，与配置在`vue.config.js`中的`outputDir`参数值相同，配置为相对路径或绝对路径均可；
   
   `${targetPath}`为 war 包内代码的相对路径，需要每个项目分别配置；
2. 配置好之后，在每次 Jenkins 发布完成之后，都会在项目根目录下生成`${parent_folder_name}.war`文件，其中`${parent_folder_name}`为当前工程所在目录名称。
