module.exports = {
  publicPath: './',
  outputDir: process.env.VUE_APP_ID,
  // 以下为esm模式依赖
  transpileDependencies: ['element-ui', 'vue-echarts', 'resize-detector'],
  devServer: {
    open: true,
    port: 8087,
    disableHostCheck: true,
    proxy: {
      '/dev': {
        target: 'http://localhost:8888/xxx',
        changeOrigin: true,
        pathRewrite: {
          '^/dev': ''
        }
      },
      // "/dev":{
      //   target: "http://localhost:3000/mock/27/hsa",
      //   changeOrigin: true,
      //   pathRewrite: {
      //     '^/dev': ''
      //   }
      // },
      // "/codes":{
      //   target: "http://192.168.129.200:8101/hsa-ips-admin",
      //   changeOrigin: true,
      //   pathRewrite: {
      //     '^/codes': ''
      //   }
      // }
      '/codes': {
        target: 'http://localhost:8888/xxx',
        changeOrigin: true,
        pathRewrite: {
          '^/codes': ''
        }
      }
      // "/codes":{
      //   target: "http://localhost:3000/mock/27/hsa",
      //   changeOrigin: true,
      //   pathRewrite: {
      //     '^/codes': ''
      //   }
      // }
    }
  },
  chainWebpack(config) {
    config
      // https://webpack.docschina.org/configuration/devtool/
      .when(
        process.env.NODE_ENV === 'development',
        config => config.devtool('eval-source-map'),
        config => config.devtool('none')
      )
  }
}
